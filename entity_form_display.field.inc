<?php

/**
 * @file
 * Provides fields, widgets and formatters for the Entity Form Display module.
 */

/**
 * Implements hook_field_info().
 */
function entity_form_display_field_info() {
  $field_types = array();
  // @todo Provide a widget for entity reference fields similar to that of
  //   Inline Entity Form module but allowing to select the form mode, or
  //   integrate with that module.
  if (module_exists('entityform')) {
    $field_types['entityform_form_display'] = array(
      'label' => t('Entityform form display'),
      'description' => t('Allows to attach an <em>Entityform Submission</em> form to the output of an entity.'),
      'instance_settings' => array(
        'type' => NULL,
        'form_mode' => NULL,
      ),
      'default_widget' => 'entityform_form_display_select',
      'default_formatter' => 'entityform_form_display',
    );
  }
  return $field_types;
}

/**
 * Implements hook_field_schema().
 */
function entity_form_display_field_schema($field) {
  $schema = array();
  if ($field['type'] == 'entityform_form_display') {
    $schema = array(
      'columns' => array(
        'type' => array(
          'description' => 'The type of the Entityform Submission',
          'type' => 'varchar',
          'length' => 255,
        ),
        'form_mode' => array(
          'description' => 'The form mode of the entity form display',
          'type' => 'varchar',
          'length' => 255,
        ),
      ),
      'indexes' => array(
        'type' => array('type'),
        'type__form_mode' => array('type', 'form_mode'),
      ),
      'foreign keys' => array(
        'type' => array(
          'table' => 'entityform_type',
          'columns' => array('type' => 'type'),
        ),
      )
    );
  }
  return $schema;
}

/**
 * Implements hook_field_is_empty().
 */
function entity_form_display_field_is_empty($item, $field) {
  if ($item['type'] == NULL && $item['form_mode'] == NULL) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Implements hook_field_widget_info().
 */
function entity_form_display_field_widget_info() {
  $widget_types = array();
  if (module_exists('entityform')) {
    $widget_types['entityform_form_display_select'] = array(
      'label' => t('Select lists'),
      'description' => t('Provides separate select lists for bundle and form mode.'),
      'field types' => array('entityform_form_display'),
    );
  }
  return $widget_types;
}

/**
 * Implements hook_field_widget_form()
 */
function entity_form_display_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $type_options = array();
  foreach (entityform_get_types() as $type_name => $type) {
    $type_options[$type_name] = entity_label('entityform_type', $type);
  }
  $element['type'] = array(
    '#title' => t('Type'),
    '#description' => t('Select the type of <em>Entityform Submission</em> whose form display will be displayed.'),
    '#type' => 'select',
    '#options' => $type_options,
    '#default_value' => $items[$delta]['type'],
    '#required' => $element['#required'],
  );

  $form_mode_options = array();
  $entity_info = entity_get_info('entityform');
  foreach ($entity_info['form modes'] as $form_mode_name => $form_mode_info) {
    $form_mode_options[$form_mode_name] = $form_mode_info['label'];
  }
  $element['form_mode'] = array(
    '#title' => t('Form mode'),
    '#type' => 'select',
    '#options' => $form_mode_options,
    '#default_value' => $items[$delta]['form_mode'],
    '#required' => $element['#required'],
  );

  return $element;
}

/**
 * Implements hook_field_formatter_info() {
 */
function entity_form_display_field_formatter_info() {
  $formatter_types = array();
  if (module_exists('entityform')) {
    $formatter_types['entityform_form_display'] = array(
      'label' => t('Form display'),
      'description' => t('The form display of the <em>Entityform Submission</em> as it is configured for the specified bundle and form mode.'),
      'field types' => array('entityform_form_display'),
    );
  }
  return $formatter_types;
}

/**
 * Implements hook_field_formatter_view().
 */
function entity_form_display_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  switch ($display['type']) {
    case 'entityform_form_display':
      foreach ($items as $delta => $item) {
        module_load_include('inc', 'entityform', 'entityform.admin');
        $entityform_submission = entity_create('entityform', array('type' => $item['type']));
        // @todo Make Entityforms compatible with entity_form()
        $entityform_submission->_form_mode = $item['form_mode'];
        // Pass the entity and entity type into drupal_get_form() to allow for
        // more targeted hook_form_alter() implementations.
        // @see entityform_edit_form()
        $element[$delta] = drupal_get_form('entityform_edit_form', $entityform_submission, 'submit', 'page', $entity_type, $entity);
        $element[$delta]['#form_mode'] = $item['form_mode'];
      }
      break;
 }

  return $element;
}