<?php

/**
 * This serves to document additional keys added to hook_entity_info().
 *
 * The following keys are provided in addition to those provided by the core
 * hook_entity_info():
 *   - form modes: An array describing the form modes for the entity type. Form
 *     modes let entity forms be displayed differently depending on the context.
 *     They are the counterpart to view modes, which allow to display the output
 *     of entities differently depending on context. For instance, a user
 *     account form can be displayed differently when editing an existing user
 *     ('account' form mode) or when registering a new user ('register' form
 *     mode). For each available form mode, administrators can configure whether
 *     it should use its own set of field widget settings, or just replicate the
 *     default settings mode, thus reducing the amount of display configurations
 *     to keep track of. Keys of the array are form mode names. Each form mode
 *     is described by an array with the following key/value pairs:
 *     - label: The human-readable name of the form mode
 *     - custom settings: A boolean specifying whether the form mode should by
 *       default use its own custom field display settings. If FALSE, entities
 *       displayed in this form mode will reuse the 'default' display settings
 *       by default (e.g. right after the module exposing the form mode is
 *       enabled), but administrators can later use the Field UI to apply custom
 *       display settings specific to the form mode.
 *     Note that in addition to defining the form modes statically as part of
 *     entity information they can also be added (and configured) in the user
 *     interface.
 *
 * @see hook_entity_info()
 */
function entity_form_display_hook_entity_info() {
  return array(
    'user' => array(
      'label' => t('User'),
      'controller class' => 'UserController',
      'base table' => 'users',
      'uri callback' => 'user_uri',
      'label callback' => 'format_username',
      'fieldable' => TRUE,
      'entity keys' => array(
        'id' => 'uid',
      ),
      'bundles' => array(
        'user' => array(
          'label' => t('User'),
          'admin' => array(
            'path' => 'admin/config/people/accounts',
            'access arguments' => array('administer users'),
          ),
        ),
      ),
      'view modes' => array(
        'full' => array(
          'label' => t('User account'),
          'custom settings' => FALSE,
        ),
      ),
      'form modes' => array(
        'account' => array(
          'label' => t('Account'),
          'custom settings' => FALSE,
        ),
        'register' => array(
          'label' => t('Register'),
          'custom settings' => TRUE,
        )
      ),
    ),
  );
}

/**
 * @todo
 */
function hook_entity_form_mode_insert($entity_type, $form_mode_name, array $form_mode) {
}

/**
 * @todo
 */
function hook_entity_form_mode_update($entity_type, $form_mode_name, array $form_mode) {
}

/**
 * @todo
 */
function hook_entity_form_mode_delete($entity_type, $form_mode_name) {
}
