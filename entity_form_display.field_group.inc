<?php

/**
 * @file
 * Integrates the Field Group module with the Entity Form Display module.
 */

/**
 * Implements hook_entity_form_mode_delete().
 *
 * @see field_group_field_attach_delete_bundle()
 */
function field_group_entity_form_mode_delete($entity_type, $form_mode_name) {
  ctools_include('export');
  $list = field_group_read_groups(array(
    'entity_type' => $entity_type,
    'mode' => $form_mode_name == 'default' ? 'form' : "form|$form_mode_name",
  ));

  // Delete the entity's entry from field_group of all entities.
  // We fetch the field groups first to assign the removal task to ctools.
  if (isset($list[$entity_type])) {
    foreach ($list[$entity_type] as $bundle_groups) {
      foreach ($bundle_groups as $groups) {
        foreach ($groups as $group) {
          ctools_export_crud_delete('field_group', $group);
        }
        unset($group);
      }
      unset($groups);
    }
    unset($bundle_groups);
  }
}

/**
 * Implements hook_entity_form_display_field_info_max_weight() on behalf of Field Group module.
 */
function field_group_entity_form_display_field_info_max_weight($entity_type, $bundle, $form_mode) {
  $weights = array();
  $bundle_settings = entity_form_display_get_bundle_settings($entity_type, $bundle);
  if (
    $form_mode != 'default' &&
    isset($bundle_settings['form_modes'][$form_mode]) &&
    empty($bundle_settings['form_modes'][$form_mode]['is_new'])
  ) {
    foreach (field_group_info_groups($entity_type, $bundle, 'form|' . $form_mode) as $group) {
      $weights[] = $group->weight;
    }
  }
  else {
    foreach (field_group_info_groups($entity_type, $bundle, 'form') as $group) {
      $weights[] = $group->weight;
    }
  }

  return $weights ? max($weights) : NULL;
}

/**
 * @todo
 *
 * @see field_group_form_pre_render()
 * @see entity_form_display_field_attach_form()
 */
function entity_form_display_field_group_form_pre_render(&$element) {
  if (isset($element['#_groups'])) {
    $element['#groups'] = $element['#_groups'];
    unset($element['#_groups']);
  }
  if (isset($element['#_fieldgroups'])) {
    $element['#fieldgroups'] = $element['#_fieldgroups'];
    unset($element['#_fieldgroups']);
  }
  if (isset($element['#_group_children'])) {
    $element['#group_children'] = $element['#_group_children'];
    unset($element['#_group_children']);
  }
  return $element;
}

/**
 * Implements hook_form_FORM_ID_alter() for 'entity_form_display_form_display_overview_form' on behalf of Field Group module.
 *
 * @see field_group_field_ui_overview_form_alter()
 * @see entity_form_display_form_display_overview_form()
 *
 * @ingroup forms
 */
function field_group_form_entity_form_display_form_display_overview_form_alter(array &$form, array &$form_state) {
  // Only start altering the form if we need to.
  if (empty($form['#fields']) && empty($form['#extra'])) {
    return;
  }

  form_load_include($form_state, 'inc', 'field_group', 'field_group.field_ui');

  $params = entity_form_display_field_group_field_ui_form_params($form);

  $form['#groups'] = array_keys($params->groups);

  $table = &$form['fields'];

  // Add a region for 'add_new' rows, but only when fields are
  // available and thus regions.
  if (isset($table['#regions'])) {
    $table['#regions'] += array(
      'add_new' => array('title' => '&nbsp;'),
    );
  }

  // Extend available parenting options.
  foreach ($params->groups as $name => $group) {
    $table['#parent_options'][$name] = $group->label;
  }
  $table['#parent_options']['_add_new_group'] = t('Add new group');

  // Update existing rows accordingly to the parents.
  foreach (element_children($table) as $name) {
    $table[$name]['parent_wrapper']['parent']['#options'] = $table['#parent_options'];
    // Inherit the value of the parent when default value is empty.
    if (empty($table[$name]['parent_wrapper']['parent']['#default_value'])) {
      $table[$name]['parent_wrapper']['parent']['#default_value'] = isset($params->parents[$name]) ? $params->parents[$name] : '';
    }
  }

  $formatter_options = field_group_field_formatter_options('form');

  $refresh_rows = isset($form_state['values']['refresh_rows']) ? $form_state['values']['refresh_rows'] : (isset($form_state['input']['refresh_rows']) ? $form_state['input']['refresh_rows'] : NULL);

  // Create the group rows and check actions.
  foreach (array_keys($params->groups) as $name) {

    // Play around with form_state so we only need to hold things
    // between requests, until the save button was hit.
    if (isset($form_state['field_group'][$name])) {
      $group = & $form_state['field_group'][$name];
    }
    else {
      $group = & $params->groups[$name];
    }

    // Check the currently selected formatter, and merge persisted values for
    // formatter settings for the group.
    // This needs to be done first, so all fields are updated before creating form elements.
    if (isset($refresh_rows) && $refresh_rows == $name) {
      $settings = isset($form_state['values']['fields'][$name]) ? $form_state['values']['fields'][$name] : (isset($form_state['input']['fields'][$name]) ? $form_state['input']['fields'][$name] : NULL);
      if (array_key_exists('settings_edit', $settings)) {
        //$group->format_type = $form_state['field_group'][$name]->format_type;
        $group = $form_state['field_group'][$name];
      }
      entity_form_display_field_group_formatter_row_update($group, $settings);
    }

    // Save the group when the configuration is submitted.
    if (!empty($form_state['values'][$name . '_formatter_settings_update'])) {
      entity_form_display_field_group_formatter_settings_update($group, $form_state['values']['fields'][$name]);
    }
    // After all updates are finished, let the form_state know.
    $form_state['field_group'][$name] = $group;

    $mode = $group->mode;
    $group->mode = 'form';
    $settings = field_group_format_settings_form($group);
    $group->mode = $mode;

    $id = strtr($name, '_', '-');
    $js_rows_data[$id] = array('type' => 'group', 'name' => $name);
    // A group cannot be selected as its own parent.
    $parent_options = $table['#parent_options'];
    unset($parent_options[$name]);
    $table[$name] = array(
      '#attributes' => array('class' => array('draggable', 'field-group'), 'id' => $id),
      '#row_type' => 'group',
      '#region_callback' => $params->region_callback,
      '#js_settings' => array('rowHandler' => 'group'),
      'human_name' => array(
        '#markup' => t('<span class="group-label">@label</span> (@name)', array(
          '@label' => t($group->label),
          '@name' => $name,
        )),
      ),
      'weight' => array(
        '#type' => 'textfield',
        '#default_value' => $group->weight,
        '#size' => 3,
        '#attributes' => array('class' => array('field-weight')),
      ),
      'parent_wrapper' => array(
        'parent' => array(
          '#type' => 'select',
          '#options' =>  $parent_options,
          '#empty_value' => '',
          '#default_value' => isset($params->parents[$name]) ? $params->parents[$name] : '',
          '#attributes' => array('class' => array('field-parent')),
          '#parents' => array('fields', $name, 'parent'),
        ),
        'hidden_name' => array(
          '#type' => 'hidden',
          '#default_value' => $name,
          '#attributes' => array('class' => array('field-name')),
        ),
      ),
    );

    $table[$name] += array(
      'format' => array(
        'type' => array(
          '#type' => 'select',
          '#options' => $formatter_options,
          '#default_value' => $group->format_type,
          '#attributes' => array('class' => array('field-group-type')),
        ),
      ),
    );

    $base_button = array(
      '#submit' => array('field_ui_display_overview_multistep_submit'),
      '#ajax' => array(
        'callback' => 'field_ui_display_overview_multistep_js',
        'wrapper' => 'field-display-overview-wrapper',
        'effect' => 'fade',
      ),
      '#field_name' => $name,
    );

    if ($form_state['formatter_settings_edit'] == $name) {
      $table[$name]['format']['#cell_attributes'] = array('colspan' => 3);
      $table[$name]['format']['format_settings'] = array(
        '#type' => 'container',
        '#attributes' => array('class' => array('field-formatter-settings-edit-form')),
        '#parents' => array('fields', $name, 'format_settings'),
        '#weight' => -5,
        'label' => array(
          '#markup' => t('Field group format:') . ' <span class="formatter-name">' . $group->format_type . '</span>',
        ),
        // Create a settings form where hooks can pick in.
        'settings' => $settings,
        'actions' => array(
          '#type' => 'actions',
          'save_settings' => $base_button + array(
              '#type' => 'submit',
              '#name' => $name . '_formatter_settings_update',
              '#value' => t('Update'),
              '#op' => 'update',
            ),
          'cancel_settings' => $base_button + array(
              '#type' => 'submit',
              '#name' => $name . '_formatter_settings_cancel',
              '#value' => t('Cancel'),
              '#op' => 'cancel',
              // Do not check errors for the 'Cancel' button.
              '#limit_validation_errors' => array(),
            ),
        ),
      );
      $table[$name]['#attributes']['class'][] = 'field-formatter-settings-editing';
      $table[$name]['format']['type']['#attributes']['class'] = array('element-invisible');
    }
    else {
      // After saving, the settings are updated here aswell. First we create
      // the element for the table cell.
      $table[$name]['settings_summary'] = array('#markup' => '');
      if (!empty($group->format_settings)) {
        $mode = $group->mode;
        $group->mode = 'form';
        $table[$name]['settings_summary'] = field_group_format_settings_summary($name, $group);
        $group->mode = $mode;
      }
      // Add the configure button.
      $table[$name]['settings_edit'] = $base_button + array(
          '#type' => 'image_button',
          '#name' => $name . '_group_settings_edit',
          '#src' => 'misc/configure.png',
          '#attributes' => array('class' => array('field-formatter-settings-edit'), 'alt' => t('Edit')),
          '#op' => 'edit',
          // Do not check errors for the 'Edit' button.
          '#limit_validation_errors' => array(),
          '#prefix' => '<div class="field-formatter-settings-edit-wrapper">',
          '#suffix' => '</div>' . l(t('delete'), $params->admin_path . '/groups/' . $name . '/delete/' . $params->mode),
        );
    }
  }

  // Additional row: add new group.
  $parent_options = $table['#parent_options'];
  unset($parent_options['_add_new_group']);
  $table['_add_new_group'] = entity_form_display_field_group_add_row('_add_new_group', $parent_options, $params);

  $table['_add_new_group'] += array(
    'format' => array(
      'type' => array(
        '#type' => 'select',
        '#options' => $formatter_options,
        '#default_value' => 'fieldset',
        '#prefix' => '<div class="add-new-placeholder">&nbsp;</div>',
      ),
      '#cell_attributes' => array('colspan' => 3),
    ),
  );

  $form['#attached']['css'][] = drupal_get_path('module', 'field_group') . '/field_group.field_ui.css';
  $form['#attached']['js'][] = drupal_get_path('module', 'field_group') . '/field_group.field_ui.js';

  // Create the settings for fieldgroup as vertical tabs (merged with DS).
  entity_form_display_field_group_field_ui_create_vertical_tabs($form, $form_state, $params);

  // Show a warning if the user has not set up required containers
  if ($form['#groups']) {

    $parent_requirements = array(
      'multipage' => array(
        'parent' => 'multipage-group',
        'message' => 'Each Multipage element needs to have a parent Multipage group element.',
      ),
      'htab' => array(
        'parent' => 'htabs',
        'message' => 'Each Horizontal tab element needs to have a parent Horizontal tabs group element.',
      ),
      'accordion-item' => array(
        'parent' => 'accordion',
        'message' => 'Each Accordion item element needs to have a parent Accordion group element.',
      ),
    );

    // On display overview tabs need to be checked.
    $parent_requirements['tab'] = array(
      'parent' => 'tabs',
      'message' => 'Each Vertical tab element needs to have a parent Vertical tabs group element.',
    );

    foreach ($form['#groups'] as $group_name) {
      $group_check = $form_state['field_group_params']->groups[$group_name];
      if (isset($parent_requirements[$group_check->format_type])) {
        $loaded_group = field_group_load_field_group($group_check->parent_name, $params->entity_type, $params->bundle, $params->mode);
        if (
          !$group_check->parent_name ||
          ($loaded_group && ($loaded_group->format_type != $parent_requirements[$group_check->format_type]['parent']))
        ) {
          drupal_set_message(t($parent_requirements[$group_check->format_type]['message']), 'warning', FALSE);
        }
      }
    }
  }

  $form['#validate'][] = 'entity_form_display_field_group_field_overview_validate';
  $form['#submit'][] = 'entity_form_display_field_group_field_overview_submit';
}

/**
 * Helper function to get the form parameters to use while
 * building the fields and display overview form.
 */
function entity_form_display_field_group_field_ui_form_params($form) {

  $params = new stdClass();
  $params->entity_type = $form['#entity_type'];
  $params->bundle = $form['#bundle'];
  $params->admin_path = _field_ui_bundle_admin_path($params->entity_type, $params->bundle);

  $params->region_callback = 'field_group_display_overview_row_region';

  $form_mode_name = $form['#form_mode'];
  $params->mode = $form_mode_name == 'default' ? 'form' : "form|$form_mode_name";

  // If there are no bundle settings for this form mode, the form mode
  // configuration has not yet been saved. In that case, clone the groups from
  // the default form mode but do not save them yet. This is the replacement
  // of the Field Group cloning from arbitrary view modes feature, as that is
  // not in-line with how the 'Default' view mode works as a fallback in the
  // rest of core.
  $form_mode_settings = entity_form_display_form_mode_settings($params->entity_type, $params->bundle);
  if ($form_mode_name == 'default' || $form_mode_settings[$form_mode_name]['has_stored_settings']) {
    $params->groups = field_group_info_groups($params->entity_type, $params->bundle, $params->mode, TRUE);
  }
  else {
    // @see field_group_field_ui_clone_field_groups()
    $source_groups = field_group_info_groups($params->entity_type, $params->bundle, 'form', TRUE);
    $params->groups = array();
    foreach ($source_groups as $source_group) {
      // Recreate the identifier and reset the id.
      $source_group->id = NULL;
      $source_group->mode = $params->mode;
      $source_group->identifier = $source_group->group_name . '|' . $source_group->entity_type . '|' . $source_group->bundle . '|' . $params->mode;
      $source_group->disabled = FALSE;
      unset($source_group->export_type, $source_group->type, $source_group->table);

      $params->groups[$source_group->group_name] = $source_group;
    }
    unset($source_group);
  }


  // Gather parenting data.
  $params->parents = array();
  foreach ($params->groups as $name => $group) {
    foreach ($group->children as $child) {
      $params->parents[$child] = $name;
    }
    unset($child);
  }
  unset($name, $group);

  return $params;
}

/**
 * Update the row so that the group variables are updated.
 * The rendering of the elements needs the updated defaults.
 * @param Object $group
 * @param array $settings
 */
function entity_form_display_field_group_formatter_row_update(& $group, $settings) {
  // if the row has changed formatter type, update the group object
  if (!empty($settings['format']['type']) && $settings['format']['type'] != $group->format_type) {
    $group->format_type = $settings['format']['type'];
    entity_form_display_field_group_formatter_settings_update($group, $settings);
  }
}

/**
 * Update handler for field_group configuration settings.
 * @param Object $group The group object
 * @param Array $settings Configuration settings
 */
function entity_form_display_field_group_formatter_settings_update(&$group, $settings) {

  // for format changes we load the defaults.
  if (empty($settings['format_settings']['settings'])) {
    $group->format_settings = _field_group_get_default_formatter_settings($group->format_type, 'form');
  }
  else {
    $group->format_type = $settings['format']['type'];
    $group->label = $settings['format_settings']['settings']['label'];
    $group->format_settings = $settings['format_settings']['settings'];
  }
}

/**
 * Helper function to add a row in the overview forms.
 */
function entity_form_display_field_group_add_row($name, $parent_options, $params) {
  return array(
    '#attributes' => array('class' => array('draggable', 'field-group', 'add-new')),
    '#row_type' => 'add_new_group',
    '#js_settings' => array('rowHandler' => 'group'),
    '#region_callback' => $params->region_callback,
    'label' => array(
      '#title_display' => 'invisible',
      '#title' => t('Label for new group'),
      '#type' => 'textfield',
      '#size' => 15,
      '#description' => t('Label'),
      '#prefix' => '<div class="label-input"><div class="add-new-placeholder">' . t('Add new group') . '</div>',
      '#suffix' => '</div>',
    ),
    'weight' => array(
      '#type' => 'textfield',
      '#default_value' => _entity_form_display_field_info_max_weight($params->entity_type, $params->bundle, $params->mode) + 3,
      '#size' => 3,
      '#title_display' => 'invisible',
      '#title' => t('Weight for new group'),
      '#attributes' => array('class' => array('field-weight')),
      '#prefix' => '<div class="add-new-placeholder">&nbsp;</div>',
    ),
    'parent_wrapper' => array(
      'parent' => array(
        '#title_display' => 'invisible',
        '#title' => t('Parent for new group'),
        '#type' => 'select',
        '#options' => $parent_options,
        '#empty_value' => '',
        '#attributes' => array('class' => array('field-parent')),
        '#prefix' => '<div class="add-new-placeholder">&nbsp;</div>',
        '#parents' => array('fields', $name, 'parent'),
      ),
      'hidden_name' => array(
        '#type' => 'hidden',
        '#default_value' => $name,
        '#attributes' => array('class' => array('field-name')),
      ),
    ),
    'group_name' => array(
      '#type' => 'textfield',
      '#title_display' => 'invisible',
      '#title' => t('Machine name for new group'),
      // This field should stay LTR even for RTL languages.
      '#field_prefix' => '<span dir="ltr">group_',
      '#field_suffix' => '</span>&lrm;',
      '#attributes' => array('dir' => 'ltr'),
      '#size' => 15,
      '#description' => t('Group name (a-z, 0-9, _)'),
      '#prefix' => '<div class="add-new-placeholder">&nbsp;</div>',
      '#cell_attributes' => array('colspan' => 1),
    ),
  );
}

/**
 * Create vertical tabs.
 */
function entity_form_display_field_group_field_ui_create_vertical_tabs(&$form, &$form_state, $params) {
  $form_state['field_group_params'] = $params;

  $entity_info = entity_get_info($params->entity_type);
  $form_modes = array();
  if ($form['#form_mode'] != 'default') {
    $form_modes['default'] = t('Default');
  }
  foreach ($entity_info['form modes'] as $form_mode => $data) {
    if ($data['custom settings'] && $params->mode != $form_mode) {
      $form_modes[$form_mode] = $data['label'];
    }
  }

  // Add additional settings vertical tab.
  if (!isset($form['additional_settings'])) {
    $form['additional_settings'] = array(
      '#type' => 'vertical_tabs',
      '#theme_wrappers' => array('vertical_tabs'),
      '#prefix' => '<div>',
      '#suffix' => '</div>',
      '#tree' => TRUE,
    );
    $form['#attached']['js'][] = 'misc/form.js';
    $form['#attached']['js'][] = 'misc/collapse.js';
  }

  // Add extra guidelines for webmaster.
  $form['additional_settings']['field_group'] = array(
    '#type' => 'fieldset',
    '#title' => t('Fieldgroups'),
    '#description' => t('<p class="fieldgroup-help">Fields can be dragged into groups with unlimited nesting. Each fieldgroup format comes with a configuration form, specific for that format type.<br />Note that some formats come in pair. These types have a html wrapper to nest its fieldgroup children. E.g. Place accordion items into the accordion, vertical tabs in vertical tab group and horizontal tabs in the horizontal tab group. There is one exception to this rule, you can use a vertical tab without a wrapper when the additional settings tabs are available. E.g. node forms.</p>'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#parents' => array('additional_settings'),
  );

  $disabled_groups = field_group_read_groups(array(), FALSE);

  // Show disabled fieldgroups, and make it possible to enable them again.
  if ($disabled_groups && isset($disabled_groups[$params->entity_type][$params->bundle][$params->mode])) {
    $form['additional_settings']['disabled_field_groups'] = array(
      '#type' => 'fieldset',
      '#title' => t('Disabled fieldgroups'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#parents' => array('additional_settings'),
    );
    $form['additional_settings']['disabled_field_groups']['overview'] = entity_form_display_field_group_disabled_groups_overview($disabled_groups[$params->entity_type][$params->bundle][$params->mode], $entity_info, $params);
  }

}

/**
 * Show an overview of all the disabled fieldgroups, and make it possible to activate them again.
 * @param $disabled_groups Array with all disabled groups.
 */
function entity_form_display_field_group_disabled_groups_overview($disabled_groups, $entity_info, $params) {
  $formatter_options = field_group_field_formatter_options('form');

  $table = array(
    '#theme' => 'table',
    '#header' => array(
      t('Field'),
      t('Widget'),
      t('Operations'),
    ),
    '#attributes' => array(
      'class' => array('field-ui-overview'),
    ),
    '#rows' => array(),
  );

  // Add all of the disabled groups as a row on the table.
  foreach ($disabled_groups as $group) {

    $summary = field_group_format_settings_summary($group->group_name, $group);

    $row = array();
    $row[] = $group->label;
    $row[] = $group->group_name;
    $row[] = $formatter_options[$group->format_type];
    $row[] = render($summary);
    $path = (isset($entity_info['bundles'][$params->bundle]['admin']['real path']) ? $entity_info['bundles'][$params->bundle]['admin']['real path'] : $entity_info['bundles'][$params->bundle]['admin']['path']);
    $row[] = l(t('Enable'), $path . '/groups/' . $group->group_name . '/enable/' . $group->mode);

    $table['#rows'][] = $row;
  }

  return $table;

}

/**
 * Form submission callback for the 'Manage form display' form of a bundle.
 *
 * @see entity_form_display_form_display_overview_form()
 * @see entity_form_display_form_display_overview_form_submit()
 */
function entity_form_display_field_group_field_overview_validate(array $form, array &$form_state) {
  $form_values = $form_state['values']['fields'];
  $entity_type = $form['#entity_type'];
  $bundle = $form['#bundle'];
  $mode = $form['#form_mode'] == 'default' ? 'form' : 'form|' . $form['#form_mode'];

  $group = $form_values['_add_new_group'];

  // Validate if any information was provided in the 'add new group' row.
  if (array_filter(array($group['label'], $group['group_name']))) {

    // Missing group name.
    if (!$group['group_name']) {
      form_set_error('fields][_add_new_group][group_name', t('Add new group: you need to provide a group name.'));
    }
    // Group name validation.
    else {
      $group_name = $group['group_name'];

      // Add the 'group_' prefix.
      if (drupal_substr($group_name, 0, 6) != 'group_') {
        $group_name = 'group_' . $group_name;
        form_set_value($form['fields']['_add_new_group']['group_name'], $group_name, $form_state);
      }

      // Invalid group name.
      if (!preg_match('!^group_[a-z0-9_]+$!', $group_name)) {
        form_set_error('fields][_add_new_group][group_name', t('Add new group: the group name %group_name is invalid. The name must include only lowercase unaccentuated letters, numbers, and underscores.', array('%group_name' => $group_name)));
      }
      if (drupal_strlen($group_name) > 32) {
        form_set_error('fields][_add_new_group][group_name', t("Add new group: the group name %group_name is too long. The name is limited to 32 characters, including the 'group_' prefix.", array('%group_name' => $group_name)));
      }

      // Group name already exists.
      if (field_group_exists($group_name, $entity_type, $bundle, $mode)) {
        form_set_error('fields][_add_new_group][group_name', t('Add new group: the group name %group_name already exists.', array('%group_name' => $group_name)));
      }
    }
  }
}

/**
 * Form submission callback for the 'Manage form display' form of a bundle.
 *
 * @see entity_form_display_form_display_overview_form()
 * @see entity_form_display_form_display_overview_form_validate()
 */
function entity_form_display_field_group_field_overview_submit(array $form, array &$form_state) {
  $form_values = $form_state['values']['fields'];
  $entity_type = $form['#entity_type'];
  $bundle = $form['#bundle'];
  $mode = $form['#form_mode'] == 'default' ? 'form' : 'form|' . $form['#form_mode'];

  // Collect children.
  $children = array_fill_keys($form['#groups'], array());
  foreach ($form_values as $name => $value) {
    if (!empty($value['parent'])) {
      // Substitute newly added fields, in case they were dragged
      // directly in a group.
      if ($name == '_add_new_field' && isset($form_state['fields_added']['_add_new_field'])) {
        $name = $form_state['fields_added']['_add_new_field'];
      }
      elseif ($name == '_add_existing_field' && isset($form_state['fields_added']['_add_existing_field'])) {
        $name = $form_state['fields_added']['_add_existing_field'];
      }
      $children[$value['parent']][$name] = $name;
    }
  }

  // Prepare storage with ctools.
  ctools_include('export');

  // Create new group.
  if (!empty($form_values['_add_new_group']['group_name'])) {
    $values = $form_values['_add_new_group'];

    $field_group_types = field_group_formatter_info();
    $formatter = $field_group_types['form'][$values['format']['type']];

    $new_group = (object) array(
      'identifier' => $values['group_name'] . '|' . $entity_type . '|' . $bundle . '|' . $mode,
      'group_name' => $values['group_name'],
      'entity_type' => $entity_type,
      'bundle' => $bundle,
      'mode' => $mode,
      'children' => isset($children['_add_new_group']) ? array_keys($children['_add_new_group']) : array(),
      'parent_name' => $values['parent'],
      'weight' => $values['weight'],
      'label' => $values['label'],
      'format_type' => $values['format']['type'],
      'disabled' => FALSE,
    );
    $new_group->format_settings = array('formatter' => isset($formatter['default_formatter']) ? $formatter['default_formatter'] : '');
    if (isset($formatter['instance_settings'])) {
      $new_group->format_settings['instance_settings'] = $formatter['instance_settings'];
    }

    $classes = _field_group_get_html_classes($new_group);
    $new_group->format_settings['instance_settings']['classes'] = implode(' ', $classes->optional);

    // Save and enable it in ctools.
    ctools_export_crud_save('field_group', $new_group);
    ctools_export_crud_enable('field_group', $new_group->identifier);

    // Store new group information for any additional submit handlers.
    $form_state['groups_added']['_add_new_group'] = $new_group->group_name;
    drupal_set_message(t('New group %label successfully created.', array('%label' => $new_group->label)));

    // Replace the newly created group in the $children array, in case it was
    // dragged directly in an existing field.
    foreach (array_keys($children) as $parent) {
      if (isset($children[$parent]['_add_new_group'])) {
        unset($children[$parent]['_add_new_group']);
        $children[$parent][$new_group->group_name] = $new_group->group_name;
      }
    }
  }

  // Update existing groups.
  foreach ($form_state['field_group_params']->groups as $group_name => $group) {
    $group->label = $form_state['field_group'][$group_name]->label;
    $group->children = array_keys($children[$group_name]);
    $group->parent_name = $form_values[$group_name]['parent'];
    $group->weight = $form_values[$group_name]['weight'];

    $old_format_type = $group->format_type;
    $group->format_type = isset($form_values[$group_name]['format']['type']) ? $form_values[$group_name]['format']['type'] : 'visible';
    if (isset($form_state['field_group'][$group_name]->format_settings)) {
      $group->format_settings = $form_state['field_group'][$group_name]->format_settings;
    }

    // If the format type is changed, make sure we have all required format settings.
    if ($group->format_type != $old_format_type) {
      $default_formatter_settings = _field_group_get_default_formatter_settings($group->format_type, 'form');
      $group->format_settings += $default_formatter_settings;
      $group->format_settings['instance_settings'] += $default_formatter_settings['instance_settings'];
    }

    ctools_export_crud_save('field_group', $group);
  }

  cache_clear_all('field_groups', 'cache_field');
}
