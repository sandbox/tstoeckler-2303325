<?php

/**
 * @file
 * Provides the ability to configure form displays for entities.
 *
 * @todo Add a 'Manage form display operation to e.g. the node type overview.
 * @todo Remove the widget settings from the field edit form.
 * @todo Allow modules to clean up when form modes are deleted.
 * @todo Various form redirects are wrong
 */

// Following core modules, field-related hook implementations are split into a
// separate file.
require_once dirname(__FILE__) . '/entity_form_display.field.inc';
// Functionality that is related to or depends on a specific module is split
// into a separate file per module.
require_once dirname(__FILE__) . '/entity_form_display.field_group.inc';

/**
 * Implements hook_help().
 */
function entity_form_display_help($path, $arg) {
  switch ($path) {
    case 'admin/help#entity_form_display':
      // @todo
      return;

    case 'admin/structure/form-modes/add':
      return '<p>' . t('Select the type of entity for which a form mode should be added.') . '</p>';
  }
}

/**
 * Implements hook_entity_info_alter().
 */
function entity_form_display_entity_info_alter(&$all_info) {
  foreach ($all_info as $type => &$type_info) {
    // Add an empty 'form modes' key to all entity types by default.
    $type_info += array('form modes' => array());

    // Merge in user-provided form modes.
    $type_info['form modes'] += entity_form_display_get_form_modes($type);

    // Provide a default value for 'custom_settings' for all given form modes.
    foreach ($type_info['form modes'] as &$form_mode) {
      $form_mode += array('custom settings' => FALSE);
    }
    unset($form_mode);
  }
  unset($type, $type_info);

  // @see https://www.drupal.org/node/2223911
  if (module_exists('xmlsitemap_menu')) {
    // Make sure this works regardless of the order the alter hook
    // implementations are called.
    if (!isset($all_info['menu_link'])) {
      xmlsitemap_menu_entity_info_alter($all_info);
    }
    $all_info['menu_link'] += array('form modes' => array());
  }
}

/**
 * @todo
 *
 * @param $entity_type
 * @return array
 */
function entity_form_display_get_form_modes($entity_type) {
  return variable_get("entity_form_display__form_modes:$entity_type", array());
}

/**
 * @todo
 *
 * @param $entity_type
 *
 * @return array|false
 */
function entity_form_display_get_form_mode($entity_type, $form_mode_name) {
  $form_modes = entity_form_display_get_form_modes($entity_type);
  return isset($form_modes[$form_mode_name]) ? $form_modes[$form_mode_name] : FALSE;
}

/**
 * @todo
 */
function entity_form_display_add_form_mode($entity_type, $form_mode_name, $form_mode) {
  $form_modes = variable_get("entity_form_display__form_modes:$entity_type", array());
  $form_modes[$form_mode_name] = $form_mode;
  variable_set("entity_form_display__form_modes:$entity_type", $form_modes);

  // Both the entity information (which is cleared by field_info_cache_clear()
  // as well), and the extra field information depend on the list of form modes.
  field_info_cache_clear();
  // The field UI local tasks depend on the list of form modes.
  // @see entity_form_display_menu()
  variable_set('menu_rebuild_needed', TRUE);

  module_invoke_all('entity_form_mode_insert', $entity_type, $form_mode_name, $form_mode);
}

/**
 * @todo
 */
function entity_form_display_update_form_mode($entity_type, $form_mode_name, $form_mode) {
  $form_modes = variable_get("entity_form_display__form_modes:$entity_type", array());
  $form_modes[$form_mode_name] = $form_mode;
  variable_set("entity_form_display__form_modes:$entity_type", $form_modes);

  // Both the entity information (which is cleared by field_info_cache_clear()
  // as well), and the extra field information depend on the list of form modes.
  field_info_cache_clear();

  module_invoke_all('entity_form_mode_update', $entity_type, $form_mode_name, $form_mode);
}

/**
 * @todo
 */
function entity_form_display_delete_form_mode($entity_type, $form_mode_name) {
  $form_modes = variable_get("entity_form_display__form_modes:$entity_type", array());
  unset($form_modes[$form_mode_name]);
  variable_set("entity_form_display__form_modes:$entity_type", $form_modes);

  // Clean-up field instances.
  foreach (field_info_instances($entity_type) as $bundle_instances) {
    foreach ($bundle_instances as $instance) {
      // Avoid needlessly updating all field instances.
      if (isset($instances['form'][$form_mode_name])) {
        unset($instances['form'][$form_mode_name]);
        field_update_instance($instance);
      }
    }
    unset($instance);
  }
  unset($bundle_instances);

  // Clean-up extra field information.
  $entity_info = entity_get_info($entity_type);
  foreach ($entity_info['bundles'] as $bundle => $bundle_info) {
    $bundle_settings = entity_form_display_get_bundle_settings($entity_type, $bundle);
    $update = FALSE;
    if (isset($bundle_settings['form_modes'][$form_mode_name])) {
      unset($bundle_settings['form_modes'][$form_mode_name]);
      $update = TRUE;
    }
    foreach ($bundle_settings['extra_fields'] as $extra_name => $extra_info) {
      if (isset($bundle_settings['extra_fields'][$extra_name][$form_mode_name])) {
        unset($bundle_settings['extra_fields'][$extra_name][$form_mode_name]);
        $update = TRUE;
      }
    }
    unset($extra_name, $extra_info);

    if ($update) {
      entity_form_display_set_bundle_settings($entity_type, $bundle, $bundle_settings);
    }
  }
  unset($bundle, $bundle_info, $bundle_settings, $update);

  module_invoke_all('entity_form_mode_delete', $entity_type, $form_mode_name);
}

/**
 * Implements hook_permission().
 */
function entity_form_display_permission() {
  $permissions['administer form modes'] = array(
    'title' => t('Administer form modes'),
  );
  foreach (entity_get_info() as $entity_type => $entity_info) {
    if (!$entity_info['fieldable']) {
      continue;
    }
    $permissions["administer $entity_type form modes"] = array(
      'title' => t('Administer %label form modes', array('%label' => $entity_info['label'])),
    );
  }
  unset($entity_type, $entity_info);

  return $permissions;
}

/**
 * Implements hook_menu_alter().
 *
 * @see field_ui_menu()
 */
function entity_form_display_menu() {
  $items = array();

  // @see \EntityDefaultUIController::hook_menu()
  $items['admin/structure/form-modes'] = array(
    'title' => 'Form modes',
    'page callback' => 'entity_form_mode_overview',
    'description' => 'Manage form modes',
    'access callback' => 'user_access',
    'access arguments' => array('administer form modes'),
    'file' => 'admin/entity_form_display.admin.form_mode.inc',
  );
  $items['admin/structure/form-modes/list'] = array(
    'title' => 'List',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );
  $items['admin/structure/form-modes/add'] = array(
    'title' => 'Add form mode',
    'page callback' => 'entity_form_display_entity_type_list',
    'access callback' => 'user_access',
    'access arguments' => array('administer form modes'),
    'type' => MENU_LOCAL_ACTION,
    'file' => 'admin/entity_form_display.admin.form_mode.inc',
  );
  $items['admin/structure/form-modes/add/%'] = array(
    'title' => 'Add form mode',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('entity_form_mode_form', 4, NULL, 'add'),
    'access callback' => 'entity_form_display_form_mode_admin_access',
    'access arguments' => array(4),
    'file' => 'admin/entity_form_display.admin.form_mode.inc',
  );
  $items['admin/structure/form-modes/manage/%/%'] = array(
    'title' => 'Edit',
    'title callback' => 'entity_form_mode_label',
    'title arguments' => array(4, 5),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('entity_form_mode_form', 4, 5, 'edit'),
    'access callback' => 'entity_form_display_form_mode_admin_access',
    'access arguments' => array(4),
    'file' => 'admin/entity_form_display.admin.form_mode.inc',
  );
  $items['admin/structure/form-modes/manage/%/%/edit'] = array(
    'title' => 'Edit',
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );

  $items['admin/structure/form-modes/manage/%/%/delete'] = array(
    'title' => 'Delete',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('entity_form_mode_delete_form', 4, 5),
    'access callback' => 'entity_form_display_form_mode_admin_access',
    'access arguments' => array(4),
    'file' => 'admin/entity_form_display.admin.form_mode.inc',
  );

  // @see field_ui_menu()
  foreach (entity_get_info() as $entity_type => $entity_info) {
    if ($entity_info['fieldable']) {
      foreach ($entity_info['bundles'] as $bundle_name => $bundle_info) {
        if (isset($bundle_info['admin'])) {
          $path = $bundle_info['admin']['path'];
          if (isset($bundle_info['admin']['bundle argument'])) {
            $bundle_arg = $bundle_info['admin']['bundle argument'];
          }
          else {
            $bundle_arg = $bundle_name;
          }

          $access = array_intersect_key($bundle_info['admin'], drupal_map_assoc(array('access callback', 'access arguments')));
          $access += array(
            'access callback' => 'user_access',
            'access arguments' => array('administer site configuration'),
          );

          $items["$path/form-display"] = array(
            'title' => 'Manage form display',
            'page callback' => 'drupal_get_form',
            'page arguments' => array(
              'entity_form_display_form_display_overview_form',
              $entity_type,
              $bundle_arg,
              'default',
            ),
            'type' => MENU_LOCAL_TASK,
            'file' => 'admin/entity_form_display.admin.form_display.inc',
            'weight' => 2,
          ) + $access;

          $items["$path/form-display/default"] = array(
            'title' => t('Default'),
            'type' => MENU_DEFAULT_LOCAL_TASK,
            'weight' => -10,
          );

          $weight = 0;
          foreach ($entity_info['form modes'] as $form_mode_name => $form_mode_info) {
            $items["$path/form-display/$form_mode_name"] = array(
                'title' => $form_mode_info['label'],
                'page callback' => 'drupal_get_form',
                'page arguments' => array(
                  'entity_form_display_form_display_overview_form',
                  $entity_type,
                  $bundle_arg,
                  $form_mode_name,
                ),
                'access callback' => 'entity_form_display_form_display_menu_access',
                'access arguments' => array_merge(array($entity_type, $bundle_arg, $form_mode_name, $access['access callback']), $access['access arguments']),
                'type' => MENU_LOCAL_TASK,
                'file' => 'admin/entity_form_display.admin.form_display.inc',
                'weight' => $weight++,
            ) + $access;
          }
          unset($form_mode_name, $form_mode_info);
        }
      }
      unset($bundle_name, $bundle_info, $path, $bundle_arg, $access, $weight);
    }
  }
  unset($entity_type, $entity_info);

  return $items;
}

/**
 * @todo
 */
function entity_form_display_form_mode_admin_access($entity_type, $account = NULL) {
  return user_access("administer $entity_type form modes", $account);
}

/**
 * Checks access for the a particular form display's configuration page.
 *
 * @param string $entity_type
 *   The entity type of the form display.
 * @param mixed $bundle
 *   A bundle string or object to retrieve the bundle name from. See
 *   field_extract_bundle() for more information.
 * @param string $form_mode
 *   The name of the form mode for this form display.
 * @param string $access_callback
 *   An additional access callback to check for if the form display exists.
 * @param ...
 *   Arguments to pass to the additional access callbacks.
 *
 * @return bool
 *   TRUE if access is granted; FALSE otherwise.
 *
 * @see _field_ui_view_mode_menu_access()
 */
function entity_form_display_form_display_menu_access($entity_type, $bundle, $form_mode, $access_callback) {
  // First, determine visibility according to the 'use custom display'
  // setting for the view mode.
  $bundle = field_extract_bundle($entity_type, $bundle);
  $form_mode_settings = entity_form_display_form_mode_settings($entity_type, $bundle);
  $visibility = ($form_mode == 'default') || $form_mode_settings[$form_mode]['custom_settings'];

  // Then, determine access according to the $access parameter. This duplicates
  // part of _menu_check_access().
  if ($visibility) {
    $all_args = func_get_args();
    $args = array_slice($all_args, 4);
    $callback = empty($access_callback) ? 0 : trim($access_callback);
    if (is_numeric($callback)) {
      return (bool) $callback;
    }
    else {
      if ($access_callback == 'user_access') {
        return (count($args) == 1) ? user_access($args[0]) : user_access($args[0], $args[1]);
      }
      elseif (function_exists($access_callback)) {
        return call_user_func_array($access_callback, $args);
      }
    }
  }
  return FALSE;
}

/**
 * Implements hook_field_extra_fields_alter().
 *
 * @see \FieldInfo::prepareExtraFields()
 */
function entity_form_display_field_extra_fields_alter(&$all_info) {
  foreach ($all_info as $entity_type => &$bundle_info) {
    $entity_info = entity_get_info($entity_type);
    $form_modes = array_merge(array('default'), array_keys($entity_info['form modes']));

    foreach ($bundle_info as $bundle => &$extra_fields) {
      $extra_fields += array('form' => array());

      $bundle_settings = entity_form_display_get_bundle_settings($entity_type, $bundle);
      $field_bundle_settings = field_bundle_settings($entity_type, $bundle);
      $field_bundle_settings = $field_bundle_settings['extra_fields']['form'];
      foreach ($extra_fields['form'] as $name => &$field_info) {
        $settings = isset($bundle_settings['extra_fields'][$name]) ? $bundle_settings['extra_fields'][$name] : array();
        // The information from field_bundle_settings() gets merged after this
        // hook is called, thus, we need to merge the default information with
        // that ourselves.
        // @see \FieldInfo::getBundleExtraFields()
        if (!isset($settings['default'])) {
          if (isset($field_bundle_settings[$name])) {
            $settings['default'] = $field_bundle_settings[$name];
          }
          else {
            $settings['default'] = array('weight' => $field_info['weight']);
          }
        }

        foreach ($form_modes as $form_mode) {
          if (isset($settings[$form_mode])) {
            $field_info['form_display'][$form_mode] = $settings[$form_mode];
          }
          else {
            $field_info['form_display'][$form_mode] = $settings['default'];
          }
        }
        unset($form_mode);
      }
      unset($name, $field_info, $settings);
    }
    unset($bundle, $extra_fields, $bundle_settings, $field_bundle_settings);
  }
  unset($entity_type, $bundle_info, $entity_info, $form_modes);
}

/**
 * Returns view mode settings in a given bundle.
 *
 * @param $entity_type
 *   The type of entity; e.g. 'node' or 'user'.
 * @param $bundle
 *   The bundle name to return view mode settings for.
 *
 * @return
 *   An array keyed by view mode, with the following key/value pairs:
 *   - custom_settings: Boolean specifying whether the view mode uses a
 *     dedicated set of display options (TRUE), or the 'default' options
 *     (FALSE). Defaults to FALSE.
 *
 * @see field_view_mode_settings()
 */
function entity_form_display_form_mode_settings($entity_type, $bundle) {
  $cache = &drupal_static(__FUNCTION__, array());

  if (!isset($cache[$entity_type][$bundle])) {
    $bundle_settings = entity_form_display_get_bundle_settings($entity_type, $bundle);
    $settings = $bundle_settings['form_modes'];
    // Include form modes for which nothing has been stored yet, but whose
    // definition in hook_entity_info() specify they should use custom settings
    // by default.
    $entity_info = entity_get_info($entity_type);
    foreach ($entity_info['form modes'] as $form_mode => $form_mode_info) {
      $settings += array($form_mode => array());
      if (!isset($settings[$form_mode]['custom_settings']) && $form_mode_info['custom settings']) {
        $settings[$form_mode]['custom_settings'] = TRUE;
      }
      $settings[$form_mode] += array(
        'custom_settings' => FALSE,
        'has_stored_settings' => FALSE,
      );
    }
    $cache[$entity_type][$bundle] = $settings;
  }

  return $cache[$entity_type][$bundle];
}

/**
 * @todo
 *
 * @param $entity_type
 * @param $bundle
 *
 * @return array
 *
 * @see field_bundle_settings()
 */
function entity_form_display_get_bundle_settings($entity_type, $bundle) {
  $bundle_settings = variable_get("entity_form_display__bundle_settings:$entity_type:$bundle", array());
  $bundle_settings += array(
    'form_modes' => array(),
    'extra_fields' => array(),
  );
  return $bundle_settings;
}

/**
 * @todo
 *
 * @param $entity_type
 * @param $bundle
 * @param $settings
 *
 * @see field_bundle_settings()
 */
function entity_form_display_set_bundle_settings($entity_type, $bundle, $settings) {
  variable_set("entity_form_display__bundle_settings:$entity_type:$bundle", $settings);
  field_info_cache_clear();
  drupal_static_reset('entity_form_display_form_mode_settings');
}

/**
 * Implements hook_menu_alter().
 */
function entity_form_display_menu_alter(&$items) {
  // @see field_ui_menu()
  foreach (entity_get_info() as $entity_type => $entity_info) {
    if ($entity_info['fieldable']) {
      foreach ($entity_info['bundles'] as $bundle_name => $bundle_info) {
        if (isset($bundle_info['admin'])) {
          $path = $bundle_info['admin']['path'];

          if (isset($bundle_info['admin']['bundle argument'])) {
            $bundle_arg = $bundle_info['admin']['bundle argument'];
          }
          else {
            $bundle_arg = $bundle_name;
          }

          // Use a different form for the instance listing.
          $items["$path/fields"]['page arguments'] = array(
            'entity_form_display_field_overview_form',
            $entity_type,
            $bundle_arg,
          );
          $items["$path/fields"]['file'] = 'admin/entity_form_display.admin.field_ui.inc';
          $items["$path/fields"]['module'] = 'entity_form_display';

          unset($items["$path/fields/%field_ui_menu/widget-type"]);

          // Adjust the weight of the 'Manage display' tab.
          $items["$path/display"]['weight'] = 3;
        }
      }
    }
  }

  // Adjust the title and weights comment field tabs.
  // @see comment_menu_alter()
  if (module_exists('comment')) {
    $items['admin/structure/types/manage/%comment_node_type/comment/fields']['weight'] = 4;

    $items['admin/structure/types/manage/%comment_node_type/comment/form-display']['title'] = 'Comment form display';
    $items['admin/structure/types/manage/%comment_node_type/comment/form-display']['weight'] = 5;

    $items['admin/structure/types/manage/%comment_node_type/comment/display']['weight'] = 6;
  }
}

/**
 * Checks whether a entity form mode with the given ID exists already.
 *
 * This is used as an exists callback for the name form element in the form mode
 * form. For general uses it is preferred to use entity_get_form_mode() instead.
 *
 * @param string $input
 *   The input of the form element.
 * @param array $element
 *   The form element
 *
 * @return bool
 *   TRUE if a form mode with the given ID exists, FALSE otherwise.
 *
 * @throws \InvalidArgumentException
 *   Throws an exception if the entity type (which is pre-set by
 *   $element['#field_prefix']) is invalid.
 */
function entity_form_mode_exists($input, array $element) {
  return (bool) entity_form_display_get_form_mode($element['#entity_type'], $input);
}

/**
 * Returns the maximum weight of all the components in an entity.
 *
 * This includes fields, 'extra_fields', and other components added by
 * third-party modules (e.g. field_group).
 *
 * @param $entity_type
 *   The type of entity; e.g. 'node' or 'user'.
 * @param $bundle
 *   The bundle name.
 * @param $mode
 *   The context for which the maximum weight is requested. Either 'form', or
 *   the name of a view mode.
 *
 * @return int|null
 *   The maximum weight of the entity's components, or NULL if no components
 *
 * @todo Document underscore (so that it is not a hook implementation).
 */
function _entity_form_display_field_info_max_weight($entity_type, $bundle, $mode) {
  $form_mode = $mode == 'default' ? 'form' : substr($mode, 5);

  $weights = array();

  // Collect weights for fields.
  foreach (field_info_instances($entity_type, $bundle) as $instance) {
    if (isset($instance['form'][$form_mode]['weight'])) {
      $weights[] = $instance['form'][$form_mode]['weight'];
    }
    else {
      $weights[] = $instance['widget']['weight'];
    }
  }
  // Collect weights for extra fields.
  foreach (field_info_extra_fields($entity_type, $bundle, 'form') as $extra) {
    $weights[] = $extra['weight'];
  }

  // Let other modules feedback about their own additions.
  $weights = array_merge($weights, module_invoke_all('entity_form_display_field_info_max_weight', $entity_type, $bundle, $form_mode));
  $max_weight = $weights ? max($weights) : NULL;

  return $max_weight;
}

/**
 * Implements hook_field_attach_form().
 */
function entity_form_display_field_attach_form($entity_type, $entity, &$form, &$form_state, $langcode) {
  // Make sure we run after _field_extra_fields_pre_render() but before
  // field_group_form_pre_render().
  $pos = array_search('_field_extra_fields_pre_render', $form['#pre_render']);
  $form['#pre_render'] = array_merge(
    array_slice($form['#pre_render'], 0, $pos + 1),
    array('entity_form_display_field_extra_fields_pre_render'),
    array_slice($form['#pre_render'], $pos + 1)
  );
  if (module_exists('field_group') && isset($entity->_form_mode)) {
    list(, , $bundle) = entity_extract_ids($entity_type, $entity);
    $settings = entity_form_display_form_mode_settings($entity_type, $bundle);
    // Fall back to the default field groups.
    if (empty($settings[$entity->_form_mode]['has_stored_settings'])) {
      $mode = 'form';
    }
    else {
      $mode = 'form|' . $entity->_form_mode;
    }

    field_group_attach_groups($form, $mode, $form_state);
    // Backup the properties as they will be overridden by
    // field_group_field_attach_form()
    $form['#_groups'] = $form['#groups'];
    $form['#_fieldgroups'] = $form['#fieldgroups'];
    $form['#_group_children'] = $form['#group_children'];
    $form['#pre_render'][] = 'entity_form_display_field_group_form_pre_render';
  }
}

function entity_form_display_field_extra_fields_pre_render($elements) {
  $entity_type = $elements['#entity_type'];
  $bundle = $elements['#bundle'];

  if (isset($elements['#type']) && $elements['#type'] == 'form' && isset($elements['#form_mode'])) {
    $extra_fields = field_info_extra_fields($entity_type, $bundle, 'form');
    foreach ($extra_fields as $name => $settings) {
      if (isset($elements[$name])) {
        $elements[$name]['#weight'] = $settings['form_display'][$elements['#form_mode']]['weight'];
      }
    }
  }

  return $elements;
}

/**
 * Returns a form array for an entity form display.
 *
 * @param array $form
 *   An associative array containing the structure of the form.
 * @param array $form_state
 *   A keyed array containing the current state of the form.
 * @param string $entity_type
 *   The entity type of the display.
 * @param object $entity
 *   The entity of the display.
 * @param string $form_mode
 *   The form mode of the display.
 *
 * @todo
 */
function entity_form_display_form(array $form = array(), array &$form_state = array(), $entity_type, $entity, $form_mode) {
  // We stuff the form mode inside $entity so that it can be reached by
  // hook_field_widget_properties_alter(). Yes, this is gross.
  $entity->_form_mode = $form_mode;
  $form_state[$entity_type] = $entity;

  // Add form keys that are required for
  // template_preprocess_entity_form_modes_form()
  list(, , $bundle) = entity_extract_ids($entity_type, $entity);
  $form += array(
    '#entity_type' => $entity_type,
    '#entity' => $entity,
    '#bundle' => $bundle,
    '#form_mode' => $form_mode,
  );

  // Fetch the appropriate entity form. The entity form is responsible for
  // calling field_attach_form().
  // @todo Can we get away with this? Or do we need to find a hacky way to
  //   detect whether field_attach_form() was called or not?
  if ($entity_form = entity_form($entity_type, $entity)) {
    $form += $entity_form;
  }

  // As hook_field_widget_properties_alter() has run, we can clean-up the entity
  // again.
  unset($entity->_form_mode);

  return $form;
}

/**
 * Implements hook_field_widget_properties_alter().
 */
function entity_form_display_field_widget_properties_alter(&$widget, $context) {
  $entity = $context['entity'];
  $instance = $context['instance'];

  if (isset($entity->_form_mode)) {
    $form_mode = $entity->_form_mode;
    if (isset($instance['form'][$form_mode])) {
      $widget = $instance['form'][$form_mode];
    }
  }
}

/**
 * Implements hook_theme().
 */
function entity_form_display_theme($existing, $type, $theme, $path) {
  return array(
    'entity_form_display' => array(
      'render element' => 'form',
      'template' => 'entity_form_display',
      'path' => drupal_get_path('module', 'entity_form_modes') . '/templates',
    ),
  );
}

/**
 * Default preprocess function for an entity form display.
 */
function template_preprocess_entity_display(&$variables) {
  $form = $variables['form'];
  if (isset($form['#form_mode'])) {
    $variables['theme_hook_suggestions'][] = 'entity_form_display__' . $form['#entity_type'];
    $variables['theme_hook_suggestions'][] = 'entity_form_display__' . $form['#entity_type'] . '__' . $form['#bundle'];
    $variables['theme_hook_suggestions'][] = 'entity_form_display__' . $form['#entity_type'] . '__' . $form['#bundle'] . '__' . $form['#form_mode'];
  }
}
