<?php

/**
 * @file
 * Administrative page and form callbacks for managing entity form displays.
 */

/**
 * Form constructor for the 'Manage form display' form of a bundle.
 *
 * In contrast to field_ui_field_overview_form() this does not allow creating or
 * removing any field instances or configuring the field instance settings or
 * field settings.
 *
 * @see entity_form_display_form_display_overview_form_validate()
 * @see entity_form_display_form_display_overview_form_submit()
 * @see field_ui_display_overview_form()
 *
 * @ingroup forms
 */
function entity_form_display_form_display_overview_form(array $form, array &$form_state, $entity_type, $bundle, $form_mode_name) {
  form_load_include($form_state, 'inc', 'field_ui', 'field_ui.admin');

  $bundle = field_extract_bundle($entity_type, $bundle);

  field_ui_inactive_message($entity_type, $bundle);
  $admin_path = _field_ui_bundle_admin_path($entity_type, $bundle);

  // Gather type information.
  $instances = field_info_instances($entity_type, $bundle);
  $field_types = field_info_field_types();
  $extra_fields = field_info_extra_fields($entity_type, $bundle, 'form');

  $form_state += array(
    'formatter_settings_edit' => NULL,
  );

  $form += array(
    '#entity_type' => $entity_type,
    '#bundle' => $bundle,
    '#form_mode' => $form_mode_name,
    '#fields' => array_keys($instances),
    '#extra' => array_keys($extra_fields),
  );

  if (empty($instances) && empty($extra_fields)) {
    drupal_set_message(t('There are no fields yet added. You can add new fields on the <a href="@link">Manage fields</a> page.', array('@link' => url($admin_path . '/fields'))), 'warning');
    return $form;
  }

  $table = array(
    '#type' => 'field_ui_table',
    '#tree' => TRUE,
    '#header' => array(
      t('Field'),
      t('Weight'),
      t('Parent'),
      array('data' => t('Widget'), 'colspan' => 3),
    ),
    '#regions' => array(
      'visible' => array('message' => t('No field is displayed.')),
    ),
    '#parent_options' => array(),
    '#attributes' => array(
      'class' => array('field-ui-overview'),
      'id' => 'field-display-overview',
    ),
    // Add Ajax wrapper.
    '#prefix' => '<div id="field-display-overview-wrapper">',
    '#suffix' => '</div>',
  );

  // Field rows.
  foreach ($instances as $name => $instance) {
    // Normalize the 'default' widget settings into the 'form' key.
    $instance += array('form' => array());
    $instance['form'] += array('default' => $instance['widget']);
    $instance['form'] += array($form_mode_name => $instance['form']['default']);

    $field = field_info_field($instance['field_name']);
    $form_display = $instance['form'][$form_mode_name];
    $table[$name] = array(
      '#attributes' => array('class' => array('draggable', 'tabledrag-leaf')),
      '#row_type' => 'field',
      '#region_callback' => 'field_ui_display_overview_row_region',
      '#js_settings' => array(
        'rowHandler' => 'field',
        'defaultFormatter' => $field_types[$field['type']]['default_widget'],
      ),
      'human_name' => array(
        '#markup' => check_plain($instance['label']),
      ),
      'weight' => array(
        '#type' => 'textfield',
        '#title' => t('Weight for @title', array('@title' => $instance['label'])),
        '#title_display' => 'invisible',
        '#default_value' => $form_display['weight'],
        '#size' => 3,
        '#attributes' => array('class' => array('field-weight')),
      ),
      'parent_wrapper' => array(
        'parent' => array(
          '#type' => 'select',
          '#title' => t('Label display for @title', array('@title' => $instance['label'])),
          '#title_display' => 'invisible',
          '#options' => $table['#parent_options'],
          '#empty_value' => '',
          '#attributes' => array('class' => array('field-parent')),
          '#parents' => array('fields', $name, 'parent'),
        ),
        'hidden_name' => array(
          '#type' => 'hidden',
          '#default_value' => $name,
          '#attributes' => array('class' => array('field-name')),
        ),
      ),
    );

    $widget_options = field_ui_widget_type_options($field['type']);
    $table[$name]['format'] = array(
      'type' => array(
        '#type' => 'select',
        '#title' => t('Widget for @title', array('@title' => $instance['label'])),
        '#title_display' => 'invisible',
        '#options' => $widget_options,
        '#default_value' => $form_display['type'],
        '#parents' => array('fields', $name, 'type'),
        '#attributes' => array('class' => array('field-widget-type')),
      ),
      'settings_edit_form' => array(),
    );

    // Formatter settings.

    // Check the currently selected formatter, and merge persisted values for
    // formatter settings.
    if (isset($form_state['values']['fields'][$name]['type'])) {
      $widget_type = $form_state['values']['fields'][$name]['type'];
    }
    else {
      $widget_type = $form_display['type'];
    }
    if (isset($form_state['formatter_settings'][$name])) {
      $settings = $form_state['formatter_settings'][$name];
    }
    else {
      $settings = $form_display['settings'];
    }
    $settings += field_info_widget_settings($widget_type);

    $instance['display'][$form_mode_name]['type'] = $widget_type;
    $widget = field_info_widget_types($widget_type);
    // Temporarily set the information into the 'widget' setting of the instance
    // so that widget settings forms work correctly.
    $original_widget = $instance['widget'];
    $instance['widget']['module'] = $widget['module'];
    $instance['widget']['settings'] = $settings;

    // Base button element for the various formatter settings actions.
    $base_button = array(
      '#submit' => array('entity_form_display_form_display_overview_multistep_submit'),
      '#ajax' => array(
        'callback' => 'field_ui_display_overview_multistep_js',
        'wrapper' => 'field-display-overview-wrapper',
        'effect' => 'fade',
      ),
      '#field_name' => $name,
    );

    $settings_form = array();
    $function = $widget['module'] . '_field_widget_settings_form';
    if (function_exists($function)) {
      $settings_form = $function($field, $instance);
      // Make the settings form's values show up at the same place in
      // $form_state['values'] where it is in the regular field settings form.
      // This ensures validation and submission handlers for widget settings
      // forms work correctly.
      // @see field_ui_field_settings_form()
      $settings_form['#parents'] = array('instance', 'widget', 'settings');
    }

    $instance['widget'] = $original_widget;

    if ($form_state['formatter_settings_edit'] == $name) {
      // We are currently editing this field's formatter settings. Display the
      // settings form and submit buttons.
      $table[$name]['format']['settings_edit_form'] = array();

      if ($settings_form) {
        $table[$name]['format']['#cell_attributes'] = array('colspan' => 3);
        $table[$name]['format']['settings_edit_form'] = array(
          '#type' => 'container',
          '#attributes' => array('class' => array('field-widget-settings-edit-form')),
          '#parents' => array('fields', $name, 'settings_edit_form'),
          'label' => array(
            '#markup' => t('Widget settings:') . ' <span class="widget-name">' . $widget['label'] . '</span>',
          ),
          'settings' => $settings_form,
          'actions' => array(
            '#type' => 'actions',
            'save_settings' => $base_button + array(
                '#type' => 'submit',
                '#name' => $name . '_widget_settings_update',
                '#value' => t('Update'),
                '#op' => 'update',
              ),
            'cancel_settings' => $base_button + array(
                '#type' => 'submit',
                '#name' => $name . '_widget_settings_cancel',
                '#value' => t('Cancel'),
                '#op' => 'cancel',
                // Do not check errors for the 'Cancel' button, but make sure we
                // get the value of the 'formatter type' select.
                '#limit_validation_errors' => array(array('fields', $name, 'type')),
              ),
          ),
        );
        $table[$name]['#attributes']['class'][] = 'field-widget-settings-editing';
      }
    }
    else {
      // Widgets do not support summaries, so we check whether there is a
      // settings form instead.
      $table[$name]['settings_summary'] = array();
      $table[$name]['settings_edit'] = array();
      if ($settings_form) {
        $table[$name]['settings_edit'] = $base_button + array(
            '#type' => 'image_button',
            '#name' => $name . '_formatter_settings_edit',
            '#src' => 'misc/configure.png',
            '#attributes' => array('class' => array('field-formatter-settings-edit'), 'alt' => t('Edit')),
            '#op' => 'edit',
            // Do not check errors for the 'Edit' button, but make sure we get
            // the value of the 'formatter type' select.
            '#limit_validation_errors' => array(array('fields', $name, 'type')),
            '#prefix' => '<div class="field-formatter-settings-edit-wrapper">',
            '#suffix' => '</div>',
          );
      }
    }
  }

  // Non-field elements.
  foreach ($extra_fields as $name => $extra_field) {
    $form_display = $extra_field['form_display'][$form_mode_name];
    $table[$name] = array(
      '#attributes' => array('class' => array('draggable', 'tabledrag-leaf')),
      '#row_type' => 'extra_field',
      '#region_callback' => 'field_ui_display_overview_row_region',
      '#js_settings' => array('rowHandler' => 'field'),
      'human_name' => array(
        '#markup' => check_plain($extra_field['label']),
      ),
      'weight' => array(
        '#type' => 'textfield',
        '#title' => t('Weight for @title', array('@title' => $extra_field['label'])),
        '#title_display' => 'invisible',
        '#default_value' => $form_display['weight'],
        '#size' => 3,
        '#attributes' => array('class' => array('field-weight')),
      ),
      'parent_wrapper' => array(
        'parent' => array(
          '#type' => 'select',
          '#title' => t('Parents for @title', array('@title' => $extra_field['label'])),
          '#title_display' => 'invisible',
          '#options' => $table['#parent_options'],
          '#empty_value' => '',
          '#attributes' => array('class' => array('field-parent')),
          '#parents' => array('fields', $name, 'parent'),
        ),
        'hidden_name' => array(
          '#type' => 'hidden',
          '#default_value' => $name,
          '#attributes' => array('class' => array('field-name')),
        ),
      ),
      'format' => array(
        'type' => array(
          '#type' => 'value',
          '#value' => 'visible',
        ),
      ),
      'settings_summary' => array(),
      'settings_edit' => array(),
    );
  }

  $form['fields'] = $table;

  // Custom display settings.
  if ($form_mode_name == 'default') {
    // Collect options and default values for the 'Custom display settings'
    // checkboxes.
    $options = array();
    $default = array();
    $entity_info = entity_get_info($entity_type);
    $form_modes = $entity_info['form modes'];
    $form_mode_settings = entity_form_display_form_mode_settings($entity_type, $bundle);
    foreach ($form_modes as $custom_form_mode_name => $custom_form_mode_info) {
      $options[$custom_form_mode_name] = $custom_form_mode_info['label'];
      if ($form_mode_settings[$custom_form_mode_name]['custom_settings']) {
        $default[] = $custom_form_mode_name;
      }
    }

    $access = (bool) $form_modes;
    $form['modes'] = array(
      '#type' => 'fieldset',
      '#title' => t('Custom display settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#access' => $access,
    );

    $form['modes']['form_modes_custom'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Use custom display settings for the following form modes'),
      '#options' => $options,
      '#default_value' => $default,
      '#access' => $access,
    );
  }

  // In overviews involving nested rows from contributed modules (i.e
  // field_group), the 'format type' selects can trigger a series of changes in
  // child rows. The #ajax behavior is therefore not attached directly to the
  // selects, but triggered by the client-side script through a hidden #ajax
  // 'Refresh' button. A hidden 'refresh_rows' input tracks the name of
  // affected rows.
  $form['refresh_rows'] = array('#type' => 'hidden');
  $form['refresh'] = array(
    '#type' => 'submit',
    '#value' => t('Refresh'),
    '#op' => 'refresh_table',
    '#submit' => array('entity_form_display_form_display_overview_multistep_submit'),
    '#ajax' => array(
      'callback' => 'field_ui_display_overview_multistep_js',
      'wrapper' => 'field-display-overview-wrapper',
      'effect' => 'fade',
      // The button stays hidden, so we hide the Ajax spinner too. Ad-hoc
      // spinners will be added manually by the client-side script.
      'progress' => 'none',
    ),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save'));

  $form['#attached']['js'][] = drupal_get_path('module', 'field_ui') . '/field_ui.js';
  $form['#attached']['css'][] = drupal_get_path('module', 'field_ui') . '/field_ui.css';
  // @todo Convert into a library.
  $form['#attached']['css'][] = drupal_get_path('module', 'entity_form_display') . '/css/entity_form_display.admin.css';

  // Add tabledrag behavior.
  $form['#attached']['drupal_add_tabledrag'][] = array('field-display-overview', 'order', 'sibling', 'field-weight');
  $form['#attached']['drupal_add_tabledrag'][] = array('field-display-overview', 'match', 'parent', 'field-parent', 'field-parent', 'field-name');

  return $form;
}

/**
 * Form submission handler for buttons in field_ui_display_overview_form().
 *
 * @see field_ui_display_overview_multistep_submit()
 */
function entity_form_display_form_display_overview_multistep_submit($form, &$form_state) {
  $trigger = $form_state['triggering_element'];
  $op = $trigger['#op'];

  switch ($op) {
    case 'edit':
      // Store the field whose settings are currently being edited.
      $field_name = $trigger['#field_name'];
      $form_state['formatter_settings_edit'] = $field_name;
      break;

    case 'update':
      // Store the saved settings, and set the field back to 'non edit' mode.
      $field_name = $trigger['#field_name'];
      $values = $form_state['values']['instance']['widget']['settings'];
      $form_state['formatter_settings'][$field_name] = $values;
      unset($form_state['formatter_settings_edit']);
      break;

    case 'cancel':
      // Set the field back to 'non edit' mode.
      unset($form_state['formatter_settings_edit']);
      break;

    case 'refresh_table':
      // If the currently edited field is one of the rows to be refreshed, set
      // it back to 'non edit' mode.
      $updated_rows = explode(' ', $form_state['values']['refresh_rows']);
      if (isset($form_state['formatter_settings_edit']) && in_array($form_state['formatter_settings_edit'], $updated_rows)) {
        unset($form_state['formatter_settings_edit']);
      }
      break;
  }

  $form_state['rebuild'] = TRUE;
}

/**
 * Form submission callback for the 'Manage form display' form of a bundle.
 *
 * @see entity_form_display_form_display_overview_form()
 * @see entity_form_display_form_display_overview_form_validate()
 */
function entity_form_display_form_display_overview_form_submit(array $form, array &$form_state) {
  $form_values = $form_state['values'];
  $entity_type = $form['#entity_type'];
  $bundle = $form['#bundle'];
  $form_mode = $form['#form_mode'];

  // Save data for 'regular' fields.
  foreach ($form['#fields'] as $field_name) {
    // Retrieve the stored instance settings to merge with the incoming values.
    $instance = field_read_instance($entity_type, $field_name, $bundle);
    $values = $form_values['fields'][$field_name];
    // Get formatter settings. They lie either directly in submitted form
    // values (if the whole form was submitted while some formatter
    // settings were being edited), or have been persisted in
    // $form_state.
    $settings = array();
    if (isset($form_state['values']['instance']['widget']['settings'])) {
      $settings = $form_state['values']['instance']['widget']['settings'];
    }
    elseif (isset($form_state['formatter_settings'][$field_name])) {
      $settings = $form_state['formatter_settings'][$field_name];
    }
    elseif (isset($instance['form'][$form_mode]['settings'])) {
      $settings = $instance['form'][$form_mode]['settings'];
    }

    $widget_info = field_info_widget_types($values['type']);
    // Only save settings actually used by the selected formatter.
    $default_settings = field_info_widget_settings($values['type']);
    $settings = array_intersect_key($settings, $default_settings);

    $instance['form'][$form_mode] = array(
      'type' => $values['type'],
      'weight' => $values['weight'],
      'settings' => $settings,
      'module' => $widget_info['module'],
    );
    if ($form_mode == 'default') {
      $instance['widget'] = $instance['form'][$form_mode];
    }

    field_update_instance($instance);
  }

  // Get current bundle settings.
  $bundle_settings = entity_form_display_get_bundle_settings($entity_type, $bundle);

  // Save data for 'extra' fields.
  foreach ($form['#extra'] as $extra_name) {
    $bundle_settings['extra_fields'][$extra_name][$form_mode] = array(
      'weight' => $form_values['fields'][$extra_name]['weight'],
    );
    if ($form_mode == 'default') {
      $field_bundle_settings = field_bundle_settings($entity_type, $bundle);
      $field_bundle_settings['extra_fields']['form'][$extra_name] = $bundle_settings['extra_fields'][$extra_name][$form_mode];
      field_bundle_settings($entity_type, $bundle, $field_bundle_settings);
    }
  }
  unset($extra_name);

  // Save form modes data.
  if ($form_mode == 'default') {
    $entity_info = entity_get_info($entity_type);
    foreach ($form_values['form_modes_custom'] as $custom_form_mode => $value) {
      // Display a message for each form mode newly configured to use custom
      // settings.
      $form_mode_settings = entity_form_display_form_mode_settings($entity_type, $bundle);
      if (!empty($value) && !$form_mode_settings[$custom_form_mode]['custom_settings']) {
        $form_mode_label = $entity_info['form modes'][$custom_form_mode]['label'];
        $path = _field_ui_bundle_admin_path($entity_type, $bundle) . "/form-display/$custom_form_mode";
        drupal_set_message(t('The %view_mode mode now uses custom display settings. You might want to <a href="@url">configure them</a>.', array(
          '%view_mode' => $form_mode_label,
          '@url' => url($path)
        )));
      }
      $bundle_settings['form_modes'][$custom_form_mode]['custom_settings'] = !empty($value);
    }
    unset($custom_form_mode, $value);
  }
  else {
    // As we do not store form displays as separate entities but instead store
    // the information in field instances, field groups, etc. directly, we need
    // some way for modules to detect whether a specific form displays has been
    // saved already or not. This can be used to provide the settings of the
    // default form display if the current form display has not been saved yet.
    $bundle_settings['form_modes'][$form_mode]['has_stored_settings'] = TRUE;
  }

  // Save updated bundle settings.
  entity_form_display_set_bundle_settings($entity_type, $bundle, $bundle_settings);

  drupal_set_message(t('Your settings have been saved.'));

}
