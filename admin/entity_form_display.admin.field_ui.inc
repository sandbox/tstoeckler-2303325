<?php

/**
 * @file
 * Administrative page and form callbacks for managing fields.
 */

/**
 * Form constructor for the 'Manage fields' form of a bundle.
 *
 * This is identical to field_ui_field_overview_form() but allows to alter this
 * form in a dedicated fashion while allowing field_ui_field_overview_form() to
 * be used as a base for both the field overview form as well as the form
 * display overview form and have modules that alter
 * field_ui_field_overview_form() still work.
 *
 * @see entity_form_display_field_overview_form_validate()
 * @see entity_form_display_field_overview_form_submit()
 * @see field_ui_field_overview_form()
 *
 * @ingroup forms
 *
 * @todo Indicate the added field somehow.
 */
function entity_form_display_field_overview_form(array $form, array &$form_state, $entity_type, $bundle) {
  form_load_include($form_state, 'inc', 'field_ui', 'field_ui.admin');
  // Do not call drupal_get_form() here to avoid multiple alteration phases.
  $form = field_ui_field_overview_form($form, $form_state, $entity_type, $bundle);

    // Remove extra fields from the table, as they are relevant for the form
  // displays only.
  foreach ($form['#extra'] as $row) {
    // It would be preferable to set #access to FALSE instead, but neither
    // field_ui_table_pre_render() nor theme_field_ui_table() respect that.
    unset($form['fields'][$row]);
  }
  // The 'Add new field' and 'Add existing field' rows' Widget type column spans
  // across the operations columns. Since we remove the Widget column, we need
  // to transfer the colspan to the Field type column.
  foreach (array('_add_new_field', '_add_existing_field') as $row) {
    if (isset($form['fields'][$row])) {
      $form['fields'][$row]['type']['#cell_attributes']['colspan'] = $form['fields'][$row]['widget_type']['#cell_attributes']['colspan'];
    }
  }
  // We cannot use array_diff() as the 'Operations' header entry is an array.
  foreach (array(t('Weight'), t('Parent'), t('Widget')) as $column) {
    $pos = array_search($column, $form['fields']['#header']);
    unset($form['fields']['#header'][$pos]);
  }
  foreach (element_children($form['fields']) as $row) {
    foreach (array('weight', 'parent_wrapper', 'widget_type') as $key) {
      // It would be preferable to set #access to FALSE instead, but neither
      // field_ui_table_pre_render() nor theme_field_ui_table() respect that. We
      // therefore exploit the hardcoded check for a 'value' element in
      // theme_field_ui_table().
      $form['fields'][$row][$key]['#type'] = 'value';
    }
  }

  // Remove obsolete tabledrag information.
  if (isset($form['#attached']['drupal_add_tabledrag'])) {
    foreach ($form['#attached']['drupal_add_tabledrag'] as $key => $tabledrag) {
      list($id) = $tabledrag;
      if ($id == 'field-overview') {
        unset($form['#attached']['drupal_add_tabledrag'][$key]);
      }
    }
  }

  // Order the fields by label.
  $labels = array();
  foreach ($form['#fields'] as $field_name) {
    $instance = field_info_instance($form['#entity_type'], $field_name, $form['#bundle']);
    $labels[$field_name] = $instance['label'];
  }
  natcasesort($labels);
  foreach (array_keys($labels) as $weight => $name) {
    $form['fields'][$name]['weight']['#value'] = $weight;
  }

  return $form;
}

/**
 * Form validation callback for the field overview form.
 *
 * @see entity_form_display_field_overview_form()
 * @see entity_form_display_field_overview_form_submit()
 * @see field_ui_field_overview_form_validate()
 */
function entity_form_display_field_overview_form_validate(array $form, array &$form_state) {
  // @see _field_ui_field_overview_form_validate_add_new()
  $field = $form_state['values']['fields']['_add_new_field'];

  if (array_filter(array($field['label'], $field['field_name'], $field['type']))) {
    if (!$field['label']) {
      form_set_error('fields][_add_new_field][label', t('Add new field: you need to provide a label.'));
    }

    if (!$field['field_name']) {
      form_set_error('fields][_add_new_field][field_name', t('Add new field: you need to provide a field name.'));
    }
    else {
      $field_name = $field['field_name'];

      $field_name = 'field_' . $field_name;
      form_set_value($form['fields']['_add_new_field']['field_name'], $field_name, $form_state);
    }

    if (!$field['type']) {
      form_set_error('fields][_add_new_field][type', t('Add new field: you need to select a field type.'));
    }
  }

  // @see _field_ui_field_overview_form_validate_add_existing()
  if (isset($form_state['values']['fields']['_add_existing_field'])) {
    $field = $form_state['values']['fields']['_add_existing_field'];

    if (array_filter(array($field['label'], $field['field_name']))) {
      if (!$field['label']) {
        form_set_error('fields][_add_existing_field][label', t('Add existing field: you need to provide a label.'));
      }

      if (!$field['field_name']) {
        form_set_error('fields][_add_existing_field][field_name', t('Add existing field: you need to select a field.'));
      }
    }
  }
}

/**
 * Form submission callback for the field overview form.
 *
 * @see entity_form_display_field_overview_form()
 * @see entity_form_display_field_overview_form_validate()
 * @see field_ui_field_overview_form_submit()
 */
function entity_form_display_field_overview_form_submit(array $form, array &$form_state) {
  $form_values = $form_state['values']['fields'];
  $entity_type = $form['#entity_type'];
  $bundle = $form['#bundle'];
  $admin_path = _field_ui_bundle_admin_path($entity_type, $bundle);

  $destinations = array();

  // Create new field.
  if (!empty($form_values['_add_new_field']['field_name'])) {
    $values = $form_values['_add_new_field'];

    $field = array(
      'field_name' => $values['field_name'],
      'type' => $values['type'],
      'translatable' => $values['translatable'],
    );
    $instance = array(
      'field_name' => $field['field_name'],
      'entity_type' => $entity_type,
      'bundle' => $bundle,
      'label' => $values['label'],
    );

    // Create the field and instance.
    try {
      field_create_field($field);
      field_create_instance($instance);

      $destinations[] = $admin_path . '/fields/' . $field['field_name'] . '/field-settings';
      $destinations[] = $admin_path . '/fields/' . $field['field_name'];

      // Store new field information for any additional submit handlers.
      $form_state['fields_added']['_add_new_field'] = $field['field_name'];
    }
    catch (Exception $e) {
      drupal_set_message(t('There was a problem creating field %label: !message', array('%label' => $instance['label'], '!message' => $e->getMessage())), 'error');
    }
  }

  // Add existing field.
  if (!empty($form_values['_add_existing_field']['field_name'])) {
    $values = $form_values['_add_existing_field'];
    $field = field_info_field($values['field_name']);
    if (!empty($field['locked'])) {
      drupal_set_message(t('The field %label cannot be added because it is locked.', array('%label' => $values['label'])), 'error');
    }
    else {
      $instance = array(
        'field_name' => $field['field_name'],
        'entity_type' => $entity_type,
        'bundle' => $bundle,
        'label' => $values['label'],
      );

      try {
        field_create_instance($instance);
        $destinations[] = $admin_path . '/fields/' . $instance['field_name'] . '/edit';
        // Store new field information for any additional submit handlers.
        $form_state['fields_added']['_add_existing_field'] = $instance['field_name'];
      }
      catch (Exception $e) {
        drupal_set_message(t('There was a problem creating field instance %label: @message.', array('%label' => $instance['label'], '@message' => $e->getMessage())), 'error');
      }
    }
  }

  if ($destinations) {
    $destination = drupal_get_destination();
    $destinations[] = $destination['destination'];
    unset($_GET['destination']);
    $form_state['redirect'] = field_ui_get_destinations($destinations);
  }
  else {
    drupal_set_message(t('Your settings have been saved.'));
  }
}
