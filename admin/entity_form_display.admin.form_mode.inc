<?php

/**
 * @file
 * Administrative page and form callbacks for managing entity form modes.
 */

/**
 * @todo
 */
function entity_form_mode_overview($destination = NULL) {
  $options = array();
  if (isset($destination)) {
    $options['query']['destination'] = $destination;
  }

  $build = array();
  foreach (entity_get_info() as $entity_type => $entity_info) {
    if (!$entity_info['fieldable']) {
      continue;
    }

    $build['table'][$entity_type] = array(
      '#theme' => 'table',
      '#prefix' => '<h2>' . $entity_info['label'] . '</h2>',
      '#header' => array(
        t('Label'),
        t('Machine name'),
        t('Custom settings'),
        array('data' => t('Operations'), 'colspan' => 2),
      ),
    );

    foreach (entity_form_display_get_form_modes($entity_type) as $form_mode_name => $form_mode) {
      $build['table'][$entity_type]['#rows'][$form_mode_name] = array(
        $form_mode['label'],
        $form_mode_name,
        $form_mode['custom settings'] ? t('Yes') : t('No'),
        l(t('Edit'), "admin/structure/form-modes/manage/$entity_type/$form_mode_name/edit", $options),
        l(t('Delete'), "admin/structure/form-modes/manage/$entity_type/$form_mode_name/delete", $options)
      );
    }

    $build['table'][$entity_type]['#rows'][] = array(array(
      'data' => l(t('Add new %label @entity-type', array(
        '%label' => $entity_info['label'],
        '@entity-type' => t('form mode'),
      )), "admin/structure/form-modes/add/$entity_type", $options + array('html' => TRUE)),
      'colspan' => 5,
    ));
  }

  return $build;
}

/**
 * @todo
 */
function entity_form_display_entity_type_list() {
  $links = array();

  foreach (entity_get_info() as $entity_type => $entity_info) {
    if (!$entity_info['fieldable']) {
      continue;
    }

    $links[] = array(
      'title' => $entity_info['label'],
      'href' => "admin/structure/form-modes/add/$entity_type",
      'localized_options' => array(),
    );
  }

  return array(
    '#theme' => 'admin_block_content',
    '#content' => $links,
  );
}

/**
 * Returns a form for entity form modes.
 */
function entity_form_mode_form(array $form, array &$form_state, $entity_type, $form_mode_name, $operation) {
  $form_state['entity_type'] = $entity_type;
  $form_state['operation'] = $operation;

  $form['entity_type'] = array(
    '#type' => 'value',
    '#value' => $entity_type,
  );

  $form['operation'] = array(
    '#type' => 'value',
    '#value' => $operation,
  );

  $form_mode = entity_form_display_get_form_mode($entity_type, $form_mode_name);
  if (!isset($form_mode)) {
    $form_mode = array(
      'label' => '',
      'custom settings' => FALSE,
    );
  }

  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#default_value' => $form_mode['label'],
  );

  $form['name'] = array(
    '#type' => 'machine_name',
    '#title' => t('Machine name'),
    '#machine_name' => array(
      'exists' => 'entity_form_mode_exists',
      'source' => array('label'),
    ),
    // Provide the entity type as context for the exists callback.
    '#entity_type' => $entity_type,
    '#default_value' => $form_mode_name,
    '#disabled' => $operation == 'edit',
  );

  $form['custom_settings'] = array(
    '#type' => 'checkbox',
    '#title' => t('Custom settings'),
    '#description' => t('If checked, entity form displays for this form mode will have custom settings by default.'),
    '#default_value' => $form_mode['custom settings'],
  );

  $form['actions']['#type'] = 'container';
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  $path = 'admin/structure/form-modes';
  // If a destination query parameter is present it generally makes sense not
  // only for the form submission to redirect there but for the cancel link, as
  // well.
  if ($_GET['destination'] && drupal_valid_path($_GET['destination'])) {
    $path = $_GET['destination'];
  }
  $form['actions']['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => $path,
  );

  return $form;
}

/**
 * Form submission callback for the entity form mode form.
 *
 * @see entity_form_mode_form()
 */
function entity_form_mode_form_submit(array $form, array &$form_state) {
  $entity_type = $form_state['entity_type'];
  $operation = $form_state['operation'];

  $form_mode_name = $form_state['values']['name'];
  if (isset($form['name']['#field_prefix'])) {
    $form_mode_name = $form['name']['#field_prefix'] . $form_mode_name;
  }

  $form_mode = array(
    'label' => $form_state['values']['label'],
    'custom settings' => (bool) $form_state['values']['custom_settings'],
  );

  if ($operation == 'add') {
    entity_form_display_add_form_mode($entity_type, $form_mode_name, $form_mode);

    drupal_set_message(t('Successfully added form mode %label.', array(
      '%label' => $form_mode['label'],
    )));
  }
  elseif ($operation == 'edit') {
    entity_form_display_update_form_mode($entity_type, $form_mode_name, $form_mode);

    drupal_set_message(t('Successfully updated form mode %label.', array(
      '%label' => $form_mode['label'],
    )));
  }

  $form_state['redirect'] = 'admin/structure/form-modes';
}

/**
 * @todo
 *
 * @see entity_form_mode_delete_form_submit()
 */
function entity_form_mode_delete_form(array $form, array &$form_state, $entity_type, $form_mode_name) {
  $form['entity_type'] = array(
    '#type' => 'value',
    '#value' => $entity_type,
  );
  $form['form_mode_name'] = array(
    '#type' => 'value',
    '#value' => $form_mode_name,
  );

  $form_mode = entity_form_display_get_form_mode($entity_type, $form_mode_name);

  return confirm_form(
    $form,
    t('Are you sure you want to delete the form mode %label', array('%label' => $form_mode['label'])),
    'admin/structure/form-modes'
  );
}

/**
 * Form submission handler for the entity form mode deletion form.
 *
 * @see entity_form_mode_delete_form()
 */
function entity_form_mode_delete_form_submit(array $form, array &$form_state) {
  $entity_type = $form_state['values']['entity_type'];
  $form_mode_name = $form_state['values']['form_mode_name'];

  $form_mode = entity_form_display_get_form_mode($entity_type, $form_mode_name);
  entity_form_display_delete_form_mode($entity_type, $form_mode_name);

  drupal_set_message(t('Successfully deleted form mode %label', array(
    '%label' => $form_mode['label'],
  )));

  $form_state['redirect'] = 'admin/structure/form-modes';
}